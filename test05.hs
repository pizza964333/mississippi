{-# LANGUAGE FlexibleInstances, ScopedTypeVariables #-}


import Data.List
import System.Random
import Data.Number.Erf
import System.IO.Unsafe

type B256 = Integer

data Focus a = Focus [a] a [a]

instance Eq (Focus Int) where
  (==) (Focus a b c) (Focus d e f) = b == e && (take 100 a == take 100 d) && (take 100 c == take 100 f)

instance Ord (Focus Int) where
  compare (Focus a b c) (Focus d e f) = (take 100 a ++ [b] ++ take 100 c) `compare` (take 100 d ++ [e] ++ take 100 f)

instance Show (Focus Int) where
  show (Focus a b c) = show $ (reverse $ take 20 a) ++ [b] ++ (take 20 c)

zeroPerm :: Focus Int
zeroPerm = Focus (map negate [1..]) 0 [1..]

randomPairPerm :: Double -> (Focus a, (Int,Int)) -> IO (Focus a, (Int,Int))
randomPairPerm (range :: Double) o@(aa,_) = do
  a <- randomRIO (-1,1)
  b <- randomRIO (-1,1)
  let c = inverf a
  let d = inverf b
  let e = (round $ c*range) :: Int
  let f = (round $ d*range) :: Int
  if isNaN c || isNaN d || isInfinite c || isInfinite d || e == f then randomPairPerm range o else do
      let [g,h] = sort [e,f]
      --print (g,h)
      return (permFocus h g aa, (g,h))

uniqFocus a = nubBy (\(a,_) (b,_) -> a == b) $ sortBy (\(a,_) (b,_) -> compare a b) a

test01 = iterateM (randomPairPerm 0.5) (zeroPerm,(0,0))  >>= (\a -> return $ take 99999 a) >>= (\a -> return $ uniqFocus a)

iterateM :: (a -> IO a) -> a -> IO [a]
iterateM f v = do
  a <- f v
  b <- unsafeInterleaveIO $ iterateM f a
  return (a : b)

-- 57

permFocus :: Int -> Int -> Focus a -> Focus a
permFocus a b o@(Focus c d e) | a < b = permFocus b a o
                              | a > 0 && b > 0  = Focus c d e3
                              | a < 0 && b < 0  = Focus c3 d e
                              | a > 0 && b < 0  = Focus c4 d e4
                              | b == 0 && a > 0 = Focus c (last i) e5
                              | a == 0 && b < 0 = Focus c5 (last p) e
 where
  (g,h) = splitAt (negate a) c
  (i,j) = splitAt a e
  (k,l) = splitAt (negate b) c2
  (m,n) = splitAt b e2
  (p,r) = splitAt (negate b) c
  e2 = (init i) ++ [last m] ++ j
  e3 = (init m) ++ [last i] ++ n
  c2 = (init g) ++ [last k] ++ h
  c3 = (init k) ++ [last g] ++ l
  c4 = (init p) ++ [last i] ++ r
  e4 = (init i) ++ [last p] ++ j
  e5 = (init i) ++ [d] ++ j
  c5 = (init p) ++ [d] ++ r


randomPerm :: IO B256
randomPerm = do
  randomRIO (0,40526919504877216755680601905432322134980384796226602145184481280000000000000-1)

-- integerToPerms :: Integer -> [Int]
integerToPerms 0 = []
integerToPerms 1 = [1,0]
integerToPerms a = integerToPermsB $ a+1


integerToPermsB a = insertAt (fromEnum c) (fromEnum $ tn - 0) $ integerToPermsA (fromEnum tn) b
 where
  list = map fct [2..]
  tn = fromInteger (tl-1)
  tl = toInteger $ (length $ takeWhile (<=a) list) + 2
  b = a `mod` (fct tn)
  c = a `div` (fct tn)

insertAt :: Int -> Int -> [Int] -> [Int]
insertAt pos val list = reverse $ b ++ [val] ++ c
 where
  (b,c) = splitAt pos $ reverse list


integerToPermsA :: Int -> Integer -> [Int]
integerToPermsA mx a = reverse $ f 0 [] a
 where
  -- f 0 _ a = f 1 [fromInteger $ a `mod` 57] (a `div` 57)
  -- f 57 l _ = l
  f n l a | n >= tl = l
  f n l a = f (n+1) ((([0..tn] \\ l) !! (fromInteger (a `mod` (tl - n)))) : l) (a `div` (tl - n))
  -- list = map fct [2..]
  list = map fct [2..]
  tn = fromInteger (tl-1)
  tl = toInteger $ ( (length $ takeWhile (<=a) list) + 2 ) `max` mx

fct x = foldr1 (*) [2..x]

rperm = randomPerm >>= (\a -> return $ integerToPerms a)



